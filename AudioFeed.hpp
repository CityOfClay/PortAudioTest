#ifndef AudioFeed_hpp
#define AudioFeed_hpp

/* ********************************************************************************* */
class AudioFeed {
  virtual void Start(){}
  virtual void Stop(){}
  virtual void Feed(std::vector<float> wav){}
};

#endif // AudioFeed_hpp
