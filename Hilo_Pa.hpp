#ifndef Hilo_Pa_hpp
#define Hilo_Pa_hpp

#include "AudioFeed.hpp"

/* ********************************************************************************* */
class Hilo_Pa : public AudioFeed {// // PortAudio callback with ring buffer, that can be fed wave sample vectors of any length.
public:
  /*******************************************************************/
  static const int Num_Channels = 1;
  static const int ChunkSize = 100;
  static const int SampleRate = 44100;
  static const int NumRingChunks = 11;//3;//2;//5;//7;//11;
  static const int RingBufSz = ChunkSize * Num_Channels * NumRingChunks;
  //float RingBuf[RingBufSz];onechannel
  float RingBuf[RingBufSz][Num_Channels];
  std::atomic_int RingGetDex, RingPutDex; // rough-grained indexes, always mod chunksize == 0.
  std::atomic_int RingPutDexNext;// fine-grained put index.

  int WavNext;
  bool KeepGoing;

  PaStream *OutStream;
  PaError OutErr;
  /*******************************************************************/
  static int PaCallback(const void *InBuf, void *OutBuf, unsigned long FramesPerBuffer, const PaStreamCallbackTimeInfo* TimeInfo, PaStreamCallbackFlags StatusFlags, void *UserData) {
    (void) TimeInfo, StatusFlags, InBuf;/* Prevent unused variable warnings. */
    Hilo_Pa *self = static_cast<Hilo_Pa*>(UserData);
    float *OutBufFloat = (float*)OutBuf;

    int RingGetDexPrev, RingGetDexNext;
    double sample;
    std::stringstream Bruce; // #include <sstream> for this
    Bruce << "PaCallback" << ", KeepGoing:" << self->KeepGoing << ", FramesPerBuffer:" << FramesPerBuffer << ", StatusFlags:" << StatusFlags << "\n";
    std::cout << Bruce.str();
    if ((StatusFlags & paOutputUnderflow) > 0LLU) {
      std::cout << "std::cout BUFFER UNDERFLOW! \n";
      printf(" PaCallback BUFFER UNDERFLOW!  \n");
      //return paComplete;
      return paContinue;
    }

    printf("BeforeThread Lock, self->RingGetDex:%d, self->RingPutDex:%d\n", self->RingGetDex.load(), self->RingPutDex.load());
    while (self->RingGetDex == self->RingPutDex) {// stall
      printf("InsideThread Lock, self->RingGetDex:%d, self->RingPutDex:%d\n", self->RingGetDex.load(), self->RingPutDex.load());
      if (!self->KeepGoing) {
        std::cout << "PaCallback abort\n" << std::flush;
        printf("PaCallback abort printf\n");
        return paComplete;
      }
      usleep(10 * 1000);// 1 milliseconds
    }
    printf("AfterThread Lock\n");
    RingGetDexPrev = self->RingGetDex;
    RingGetDexNext = RingGetDexPrev + ChunkSize;// Increase by chunk size.

    if (false) {
      std::cout << "***** PaCallback Chunk: " << "\n";
      for (int pcnt=RingGetDexPrev; pcnt<RingGetDexNext; pcnt++) {// print data
        std::stringstream Bruce; // #include <sstream> for this
        Bruce << "READER pcnt:[" << pcnt << "] RingBuf:(" << self->RingBuf[pcnt][0] << ")\n";// to do: read data here
        std::cout << Bruce.str();
      }
    }
    std::stringstream Bruce2;
    Bruce2 << "PLAYING\n";
    std::cout << Bruce2.str();
    if (false) {// All of this can be replaced with a mem copy.
      float sample;
      for (int RingCnt=RingGetDexPrev; RingCnt<RingGetDexNext; RingCnt++) {// output data
        //sample = (float)self->RingBuf[RingCnt];onechannel
        for (int ChnlCnt=0; ChnlCnt<Num_Channels; ChnlCnt++) {
          sample = (float)self->RingBuf[RingCnt][ChnlCnt];
          *OutBufFloat++ = sample;
        }
      }
    } else {
      std::memcpy(OutBufFloat, (self->RingBuf[RingGetDexPrev]), ChunkSize*Num_Channels*sizeof(float));
    }

    if ((RingGetDexNext) >= RingBufSz){RingGetDexNext=0;}// wrap
    self->RingGetDex = RingGetDexNext;
    return paContinue;
  }
  /*******************************************************************/
  static void StreamFinished(void* UserData) {/* This routine is called by portaudio when playback is done. */
    Hilo_Pa *self = (Hilo_Pa*)UserData;
    self->KeepGoing = false;
    printf("Stream Completed in Hilo_Pa StreamFinished: %d\n", self->KeepGoing);
  }
  /*******************************************************************/
  void Start() override {
    signed long writeable = 0;
    this->Init();

    PaStreamParameters OutputParameters;

    OutErr = Pa_Initialize();
    if( OutErr != paNoError ) { ReportError(OutErr, 0); return; }

    OutputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (OutputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      ReportError(OutErr, 1); return;
    }
    OutputParameters.channelCount = Num_Channels; /* stereo output */
    OutputParameters.sampleFormat = paFloat32;    /* 32 bit floating point output */
    //OutputParameters.suggestedLatency = 0.050;
    OutputParameters.suggestedLatency = Pa_GetDeviceInfo( OutputParameters.device )->defaultLowOutputLatency;
    printf("OutputParameters.suggestedLatency:%f\n", OutputParameters.suggestedLatency);// outputParameters.suggestedLatency:0.008707
    OutputParameters.hostApiSpecificStreamInfo = NULL;

    OutErr = Pa_OpenStream(
            &OutStream, NULL, /* Output, no input */
            &OutputParameters, SampleRate, ChunkSize,
            paClipOff,      /* we won't output out of range samples so don't bother clipping them */
            PaCallback, this);

    if( OutErr != paNoError ) { ReportError(OutErr, 3); return; }

    writeable = Pa_GetStreamWriteAvailable(OutStream);
    printf("writeable:%ld\n", writeable);

    OutErr = Pa_SetStreamFinishedCallback(OutStream, &StreamFinished);
    if(OutErr != paNoError) { ReportError(OutErr, 4); return; }

    OutErr = Pa_StartStream(OutStream);
    if(OutErr != paNoError) { ReportError(OutErr, 5); return; }
  }
  /*******************************************************************/
  void Init() {
    this->KeepGoing = true;
    this->WavNext = ChunkSize;
    this->RingGetDex = this->RingPutDex = 0; // rough-grained indexes, always mod chunksize == 0.
    this->RingPutDexNext = 0;// fine-grained put index.
    // for (int cnt=0;cnt<RingBufSz; cnt++) { this->RingBuf[cnt] = 0.0; }onechannel
    for (int cnt=0;cnt<RingBufSz; cnt++) {
      for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
        this->RingBuf[cnt][chcnt] = 0.0;
      }
    }
    // memset(this->RingBuf, 0, RingBufSz);
    std::cout << "Init() \n";
  }
  /*******************************************************************/
  void Stop() override {
    this->Drain();
    this->UnInit();
  }
  /*******************************************************************/
  void UnInit() {
    this->KeepGoing = false;
    std::cout << "UnInit() " << ", KeepGoing:" << this->KeepGoing << "\n";
  }
  /*******************************************************************/
  void Feed(std::vector<float> wave) override {// Think of this as a loop.  Integrates ring buffer and odd-sized wave vectors.
    // At this point WavNext should be the offset from the beginning of the array that we run up *to*
    // WavNext *can* be beyond the end of the next array, if next array length is < ChunkSize.
    double value;
    int RingCnt = this->RingPutDexNext;// A dereference for RingPutDexNext.
    int WavPrev = 0, WavDex;
    int WavSize = wave.size();// Only need to dereference wave size once.
    std::cout << "WavSize:"<< WavSize <<" \n";
    while (this->WavNext <= WavSize) {
      for (WavDex = WavPrev; WavDex<this->WavNext; WavDex++) {// copy chunk, or chunklet inherited from previous call.
        //printf("WavDex:%d, WavNext:%d\n", WavDex, this->WavNext);
        value = wave[WavDex];
        //RingBuf[RingCnt++] = value;onechannel
        for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
          this->RingBuf[RingCnt][chcnt] = value;// Copy one channel to all.
        }
        RingCnt++;
      }
      printf("WavDex:%d, WavNext:%d\n", WavDex, this->WavNext);
      WavPrev = this->WavNext;
      this->WavNext += ChunkSize;
      if (RingCnt >= RingBufSz) { RingCnt = 0; } // wrap
      printf("Before Lock, RingCnt:%d, this->RingGetDex:%d\n", RingCnt, this->RingGetDex.load());
      while (RingCnt == this->RingGetDex){// Stall until player is ready.  snox
        // to do: put timeout condition to prevent lockup?
      }
      printf("After Lock, this->RingPutDex:%d\n", this->RingPutDex.load());
      this->RingPutDex = RingCnt;
      //usleep(100 * 1000);// milliseconds
    }
    for (WavDex = WavPrev; WavDex<WavSize; WavDex++) {// copy remainder, last bit of array, which is less than ChunkSize long.
      value = wave[WavDex];
      //RingBuf[RingCnt++] = value;onechannel
      for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
        this->RingBuf[RingCnt][chcnt] = value;
      }
      RingCnt++;
    }
    this->RingPutDexNext = RingCnt;
    this->WavNext -= WavSize; // After end of remainder, set up index for next call to Feed().
  }
  /*******************************************************************/
  void Drain() {// Close out last bit of odd-sized remainder.
    printf("Drain.............\n");
    if (this->WavNext < ChunkSize) {
      int End = this->RingPutDexNext + this->WavNext;
      for (int rcnt=this->RingPutDexNext; rcnt<End; rcnt++) {
        // RingBuf[rcnt] = 0;onechannel
        for (int chcnt=0;chcnt<Num_Channels; chcnt++) {
          this->RingBuf[rcnt][chcnt] = 0.0;
        }
      }// Pad with zeros.
      if (End >= RingBufSz) { End=0; }// wrap
      while (this->RingGetDex == End) {// stall.  RingPutDex is not allowed to overtake RingGetDex.
        // to do: put timeout condition to prevent lockup?
      }
      this->RingPutDex = End;
    }
  }
  /*******************************************************************/
  void ReportError(PaError OutErr, int Tag) {
    printf("****************************** ReportError:%d\n", Tag);
    fprintf(stderr, "An error occured while using the portaudio stream\n" );
    fprintf(stderr, "Error number: %d\n", OutErr);
    fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(OutErr) );
    if (OutErr == paUnanticipatedHostError) {// Print more information about the error.
      const PaHostErrorInfo *hostErrorInfo = Pa_GetLastHostErrorInfo();
      fprintf( stderr, "Host API error = #%ld, hostApiType = %d\n", hostErrorInfo->errorCode, hostErrorInfo->hostApiType );
      fprintf( stderr, "Host API error = %s\n", hostErrorInfo->errorText );
    }
    Pa_Terminate();
    exit(EXIT_FAILURE);
  }
};

#endif // Hilo_Pa_hpp
