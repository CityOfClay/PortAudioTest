#include <iostream>
// #include "blocking.hpp"

//#include "portaudio.h"
//using namespace std;

// PortAudio test
/*
 * $Id$
 *
 * This program uses the PortAudio Portable Audio Library.
 * For more information see: http://www.portaudio.com/
 * Copyright (c) 1999-2000 Ross Bencina and Phil Burk
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * The text above constitutes the entire PortAudio license; however,
 * the PortAudio community also makes the following non-binding requests:
 *
 * Any person wishing to distribute modifications to the Software is
 * requested to send the modifications to the original developer so that
 * they can be incorporated into the canonical version. It is also
 * requested that these non-binding requests be included along with the
 * license above.
 */
#include <stdio.h>
#include <math.h>

#include <cstdlib>
#include <ctime>

#include "portaudio.h"

#include "AudioLab.hpp"
#include "Hilo.hpp"
#include "Hilo_Pa.hpp"

#define NUM_SECONDS   (5)
#define SAMPLE_RATE   (44100)
#define FRAMES_PER_BUFFER  (64)

#ifndef M_PI
#define M_PI  (3.14159265)
#endif

#define TABLE_SIZE   (200)
typedef struct {
  float sine[TABLE_SIZE];
  int left_phase;
  int right_phase;
  char message[20];
}
paTestData;

/* This routine will be called by the PortAudio engine when audio is needed.
** It may called at interrupt level on some machines so don't do anything
** that could mess up the system like calling malloc() or free().
*/
static int patestCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData )
{
  paTestData *data = (paTestData*)userData;
  float *out = (float*)outputBuffer;
  unsigned long i;

  (void) timeInfo; /* Prevent unused variable warnings. */
  (void) statusFlags;
  (void) inputBuffer;

  for( i=0; i<framesPerBuffer; i++ ) {
    *out++ = data->sine[data->left_phase];  /* left */
    *out++ = data->sine[data->right_phase];  /* right */
    data->left_phase += 1;
    if( data->left_phase >= TABLE_SIZE ){
      data->left_phase -= TABLE_SIZE;
    }
    data->right_phase += 3; /* higher pitch so we can distinguish left and right. */
    if( data->right_phase >= TABLE_SIZE ){
      data->right_phase -= TABLE_SIZE;
    }
  }

  return paContinue;
}

/*
 * This routine is called by portaudio when playback is done.
 */
static void StreamFinished( void* userData ) {
  paTestData *data = (paTestData *) userData;
  printf( "Stream Completed in main.cpp: %s\n", data->message );
}
/*******************************************************************/
void TestFeeder() {
  std::vector<float> wav;
  for (int cnt=0;cnt<1024*400;cnt++){
    wav.push_back(cnt);
  }
  int NumSeconds = 1;
  Audio audio;
  audio.AsyncStart();
  audio.AsyncFeed(wav);
  printf("Play for %d seconds.\n", NumSeconds);
  // Pa_Sleep(NumSeconds * 1000);
  usleep(NumSeconds * 1000);
  audio.AsyncStop();
  printf("audio.AsyncStop(); done.\n");
}
/*******************************************************************/
void TestThreader() {
  std::vector<float> wav;
  int NumSamples = Hilo::ChunkSize*41;
  NumSamples = 41;
  // NumSamples = 7;
  for (int cnt=0; cnt<NumSamples; cnt++){
    wav.push_back(cnt);
  }
  int NumSeconds = 1;
  Hilo_Ring hilo;
  hilo.Start();
  printf("Play for %d seconds.\n", NumSeconds);
  hilo.Feed(wav);
  usleep(NumSeconds * 1000 * 1000);
  hilo.Stop();
  printf("hilo.Stop(); done.\n");
}
/*******************************************************************/
void TestFeeder2() {
  // srand(time(NULL));
  std::srand(std::time(nullptr));
  std::vector<float> wav;
  float value;
  int NumSamples = Hilo_Wave::ChunkSize*41;
  NumSamples = 41;
  // NumSamples = 7;
  int randsize;
  int RandRange = 7;
  int MinSize = 1;
  int NumWaves = 6;
  value = 0;
  Hilo_Wave hilo;
  hilo.Start();
  for (int wcnt=0; wcnt<NumWaves; wcnt++) {
    // get random number here
    // randsize = wcnt + MinSize;
    int random_variable = MinSize + (std::rand() % RandRange);
    NumSamples = random_variable;//randsize;
    for (int scnt=0;scnt<NumSamples;scnt++) {
      wav.push_back(value++);
    }
    hilo.Feed(wav);
    wav.clear();
  }
  hilo.Stop();
  std::cout << "IN value:" << value << "\n";
  printf("TestFeeder2(); done.\n");
}
/*******************************************************************/
void TestFeeder3() {
  // srand(time(NULL));
  int now = std::time(nullptr);
  //now = 1607386508;
  //now = 1607434177;// makes even 21 integers

  std::cout << "now:" << now << "\n";
  std::srand(now);
  std::vector<float> wav;
  float value;
  int NumSamples;
  int RandRange = 13;//7;//3;//
  int MinSize = 1;
  int NumWaves = 5;//11;//5;//9;//7;//5;//3;//
  value = 0;
  std::cout << "TestFeeder3(); start ******************************************************************************************************\n";
  Hilo hilo;
  hilo.Start();// threaded
  //hilo.Init();
  for (int wcnt=0; wcnt<NumWaves; wcnt++) {
    // get random number here
    int random_variable = MinSize + (std::rand() % RandRange);
//    random_variable = MinSize + wcnt;
//    if (wcnt & 0x1 == 0x1) {
//      random_variable = 5;
//    } else {
//      random_variable = 2;
//    }
    NumSamples = random_variable;
    for (int scnt=0;scnt<NumSamples;scnt++) {
      wav.push_back(value++);
    }
    // do feed here,
    hilo.Feed(wav);
    wav.clear();
  }
  hilo.Stop();// threaded
  //hilo.UnInit();
  std::cout << "IN value:" << value << "\n";
  printf("TestFeeder3(); done.\n");
}
/*******************************************************************/
void TestFeeder_Pa() {
  int now = std::time(nullptr);
  std::cout << "now:" << now << "\n";
  std::srand(now);
  std::vector<float> wav;
  int SampleCnt = 0;
  float value;
  float TwoPi = M_PI * 2.0;
  float fraction = 440.0 / (float)(Hilo_Pa::SampleRate);// frequency (A440) over sample rate
  fraction *= TwoPi;
  int NumSamples;
  int MinSize = 100;
  int RandRange = 100;//7;//3;//
  //int NumWaves = 1000;//11;//5;//9;//7;//5;//3;//
  int NumWaves = 100;//5;//9;//7;//5;//3;//
  value = 0;
  std::cout << "TestFeeder_Pa(); start ******************************************************************************************************\n";
  Hilo_Pa hilo;
  hilo.Start();// threaded
  for (int wcnt=0; wcnt<NumWaves; wcnt++) {
    // int random_variable = MinSize + (std::rand() % RandRange);// get random number here
    int random_variable = MinSize + (RandRange/2);// get random number here
    NumSamples = random_variable;
    printf("Main NumSamples:%d\n", NumSamples);
    for (int scnt=0;scnt<NumSamples;scnt++) {
      value = sin(((float)SampleCnt) * fraction);
      wav.push_back(value);
      SampleCnt++;
    }
    //usleep(1 * 100 * 1000);// to create buffer underflow
    hilo.Feed(wav);
    wav.clear();
  }
  hilo.Stop();// threaded
  hilo.Drain();
  std::cout << "IN SampleCnt:" << SampleCnt << "\n";
  printf("TestFeeder_Pa(); done.\n");
  usleep(1 * 100 * 1000);// see if this is needed to drain.
}
/*******************************************************************/
int main(void) { // http://portaudio.com/docs/v19-doxydocs/paex__sine_8c_source.html
  if (true){
    TestFeeder_Pa();
    //TestFeeder3();
    //TestFeeder2();
    //TestThreader();
    //TestFeeder();
    //Audio::Blocking();
    //Audio::AudioAsync();
    return 0;
  }
  PaStreamParameters outputParameters;
  PaStream *stream;
  PaError err;
  paTestData data;
  int i;

  printf("PortAudio Test: output sine wave. SR = %d, BufSize = %d\n", SAMPLE_RATE, FRAMES_PER_BUFFER);

  /* initialise sinusoidal wavetable */
  for( i=0; i<TABLE_SIZE; i++ ) {
    data.sine[i] = (float) sin( ((double)i/(double)TABLE_SIZE) * M_PI * 2. );
  }
  data.left_phase = data.right_phase = 0;

  err = Pa_Initialize();
  if( err != paNoError ) {
    goto error;
  }

  outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
  if (outputParameters.device == paNoDevice) {
    fprintf(stderr,"Error: No default output device.\n");
    goto error;
  }
  outputParameters.channelCount = 2;       /* stereo output */
  outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
  outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
  outputParameters.hostApiSpecificStreamInfo = NULL;

  err = Pa_OpenStream(
          &stream,
          NULL, /* no input */
          &outputParameters,
          SAMPLE_RATE,
          FRAMES_PER_BUFFER,
          paClipOff,      /* we won't output out of range samples so don't bother clipping them */
          patestCallback,
          &data );
  if( err != paNoError ) {
    goto error;
  }

  sprintf( data.message, "No Message" );
  err = Pa_SetStreamFinishedCallback( stream, &StreamFinished );
  if( err != paNoError ) {
    goto error;
  }

  err = Pa_StartStream( stream );
  if( err != paNoError ) {
    goto error;
  }

  printf("Play for %d seconds.\n", NUM_SECONDS );
  Pa_Sleep( NUM_SECONDS * 1000 );

  err = Pa_StopStream( stream );
  if( err != paNoError ) {
    goto error;
  }

  err = Pa_CloseStream( stream );
  if( err != paNoError ) {
    goto error;
  }

  Pa_Terminate();
  printf("Test finished.\n");

  return err;
error:
  Pa_Terminate();
  fprintf( stderr, "An error occured while using the portaudio stream\n" );
  fprintf( stderr, "Error number: %d\n", err );
  fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( err ) );
  return err;
}
