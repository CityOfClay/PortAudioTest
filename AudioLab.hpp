#ifndef AudioLab_hpp
#define AudioLab_hpp

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <stdalign.h>
#include <cstring>
#include <vector>
#include <thread> // std::thread
#include <sstream>
#include <atomic>
#include "portaudio.h"

#define NUM_SECONDS         (5)
#define SAMPLE_RATE         (44100)
#define FRAMES_PER_BUFFER   (1024)
#define NUM_CHANNELS   (2)

#ifndef M_PI
#define M_PI  (3.14159265)
#endif

#define TABLE_SIZE   (200) // sine table size

#include "AudioFeed.hpp"

typedef struct {
  float sine[TABLE_SIZE];
  int left_phase;
  int right_phase;
  int AbsCnt;
  char message[20];
} PaTestData;

/* ********************************************************************************* */
class Audio : public AudioFeed {
public:
  PaStream *OutStream;
  PaError OutErr;
  /* This routine will be called by the PortAudio engine when audio is needed.
  ** It may called at interrupt level on some machines so don't do anything
  ** that could mess up the system like calling malloc() or free().
  */
  static int PaTestCallback(const void *InBuf, void *OutBuf, unsigned long FramesPerBuffer,
                            const PaStreamCallbackTimeInfo* TimeInfo, PaStreamCallbackFlags StatusFlags, void *UserData) {
    PaTestData *data = (PaTestData*)UserData;
    float *out = (float*)OutBuf;
    unsigned long cnt;

    (void) TimeInfo; /* Prevent unused variable warnings. */
    (void) StatusFlags;
    (void) InBuf;

    for(cnt=0; cnt<FramesPerBuffer; cnt++ ) {
      *out++ = data->sine[data->left_phase];  /* left */
      *out++ = data->sine[data->right_phase];  /* right */
      data->left_phase += 1;
      if( data->left_phase >= TABLE_SIZE ) {
        data->left_phase -= TABLE_SIZE;// wrap
      }
      data->right_phase += 3; /* higher pitch so we can distinguish left and right. */
      if ( data->right_phase >= TABLE_SIZE ) {
        data->right_phase -= TABLE_SIZE;// wrap
      }
    }

    data->AbsCnt++;
    //printf("PaTestCallback:%d\n", data->AbsCnt);
    printf("FramesPerBuffer:%d\n", FramesPerBuffer);
    if (false){
      if (data->AbsCnt>150){
        return paComplete;
      }
    }

    return paContinue;
  }
  /*******************************************************************/
  static void StreamFinished(void* UserData) {/* This routine is called by portaudio when playback is done. */
    PaTestData *data = (PaTestData *)UserData;
    printf("Stream Completed in StreamFinished: %s\n", data->message);
  }
  /*******************************************************************/
  static int AudioAsync() { // http://portaudio.com/docs/v19-doxydocs/paex__sine_8c_source.html
    PaStreamParameters OutputParameters;
    PaStream *stream;
    PaError err;
    PaTestData data;
    int i;
    signed long writeable = 0;
    int NumSeconds = 1;

    printf("PortAudio Test: output sine wave. SR = %d, BufSize = %d\n", SAMPLE_RATE, FRAMES_PER_BUFFER);

    /* initialise sinusoidal wavetable */
    for( i=0; i<TABLE_SIZE; i++ ) {
      data.sine[i] = (float) sin( ((double)i/(double)TABLE_SIZE) * M_PI * 2. );
    }
    data.left_phase = data.right_phase = 0;
    data.AbsCnt=0;

    err = Pa_Initialize();
    if( err != paNoError ) {
      goto error0;
    }

    OutputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (OutputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      goto error0;
    }
    OutputParameters.channelCount = NUM_CHANNELS;       /* stereo output */
    OutputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    OutputParameters.suggestedLatency = Pa_GetDeviceInfo(OutputParameters.device)->defaultLowOutputLatency;
    OutputParameters.hostApiSpecificStreamInfo = NULL;

    err = Pa_OpenStream(
            &stream, NULL, /* no input */
            &OutputParameters,
            // SAMPLE_RATE, FRAMES_PER_BUFFER,
            SAMPLE_RATE, paFramesPerBufferUnspecified,
            paClipOff,      /* we won't output out of range samples so don't bother clipping them */
            PaTestCallback,
            &data);

    writeable = Pa_GetStreamWriteAvailable(stream);
    printf("writeable:%ld\n", writeable);

    if( err != paNoError ) { goto error0; }

    sprintf(data.message, "No Message");
    err = Pa_SetStreamFinishedCallback( stream, &StreamFinished );
    if( err != paNoError ) { goto error0; }

    err = Pa_StartStream( stream );
    if( err != paNoError ) { goto error0; }

    printf("Play for %d seconds.\n", NumSeconds);
    Pa_Sleep(NumSeconds * 1000);

//    for (int cnt=0;cnt<100;cnt++){
//       char *bleh = (char*)malloc(10000);
//       free(bleh);
//       printf("bleh!\n");
//    }
//    Pa_Sleep(NumSeconds * 1000 );

    err = Pa_StopStream( stream );
    if( err != paNoError ) { goto error0; }

    err = Pa_CloseStream( stream );
    if( err != paNoError ) { goto error0; }

    Pa_Terminate();
    printf("Test finished.\n");

    return err;
error0:
    Pa_Terminate();
    fprintf( stderr, "An error occured while using the portaudio stream\n" );
    fprintf( stderr, "Error number: %d\n", err );
    fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( err ) );
    return err;
  }

  /*******************************************************************/
  static const int AsyncRingBufSz = FRAMES_PER_BUFFER * NUM_CHANNELS * 4;
  float AsyncRingBuf[AsyncRingBufSz];
  int AsyncGetDex, AsyncPutDex;
  bool KeepGoing;
  /*******************************************************************/
  static int PaCallback2(const void *InBuf, void *OutBuf, unsigned long FramesPerBuffer, const PaStreamCallbackTimeInfo* TimeInfo, PaStreamCallbackFlags StatusFlags, void *UserData) {
    printf("PaCallback2 start\n");
    Audio *audio = (Audio*)UserData;
    float *out = (float*)OutBuf;
    (void) TimeInfo; (void) StatusFlags; (void) InBuf;/* Prevent unused variable warnings. */
    unsigned long GetDexPrev, GetDexNext;
    // or just this?
    // stall until write catches up?  if this loop executes even once it is probably a disaster.
    while (audio->AsyncGetDex == audio->AsyncPutDex){
      // Pa_Sleep(10);
      // usleep(10 * 1000);// 10 milliseconds = 1/100 second is tolerable
      //printf("audio->AsyncPutDex:%d\n", audio->AsyncPutDex);
      if (!audio->KeepGoing){ printf("PaCallback2 abort\n"); return paComplete;}
    }
    GetDexPrev = audio->AsyncGetDex;
    if ((GetDexNext = GetDexPrev + FRAMES_PER_BUFFER) >= AsyncRingBufSz){GetDexNext=0;}// wrap

    memcpy(out, &(audio->AsyncRingBuf[GetDexPrev]), sizeof(float) * FRAMES_PER_BUFFER * NUM_CHANNELS);
    audio->AsyncGetDex = GetDexNext;
    printf("FramesPerBuffer:%d\n", FramesPerBuffer);
    printf("PaCallback2 stop\n");
    return paContinue;
  }
  /*******************************************************************/
  static void StreamFinished2(void* UserData) {/* This routine is called by portaudio when playback is done. */
    Audio *audio = (Audio *)UserData;
    audio->KeepGoing = false;
    printf("Stream Completed in StreamFinished2: %d\n", audio->KeepGoing);
  }
  /*******************************************************************/
  void AsyncStart() {
    this->AsyncGetDex = this->AsyncPutDex = 0;
    memset(this->AsyncRingBuf, 0, AsyncRingBufSz);

    PaStreamParameters OutputParameters;

    OutErr = Pa_Initialize();
    if( OutErr != paNoError ) { ReportError(OutErr); return; }

    OutputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (OutputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      ReportError(OutErr); return;
    }
    OutputParameters.channelCount = NUM_CHANNELS; /* stereo output */
    OutputParameters.sampleFormat = paFloat32;    /* 32 bit floating point output */
    OutputParameters.suggestedLatency = 0.050; // Pa_GetDeviceInfo( OutputParameters.device )->defaultLowOutputLatency;
    OutputParameters.hostApiSpecificStreamInfo = NULL;

    OutErr = Pa_OpenStream(
            &OutStream,
            NULL, /* no input */
            &OutputParameters,
            SAMPLE_RATE, FRAMES_PER_BUFFER,
            paClipOff,      /* we won't output out of range samples so don't bother clipping them */
            PaCallback2,
            this);

    if( OutErr != paNoError ) { ReportError(OutErr); return; }

//    writeable = Pa_GetStreamWriteAvailable(stream);
//    printf("writeable:%ld\n", writeable);

    OutErr = Pa_SetStreamFinishedCallback(OutStream, &StreamFinished2);
    if(OutErr != paNoError) { ReportError(OutErr); return; }

    OutErr = Pa_StartStream(OutStream);
    if(OutErr != paNoError) { ReportError(OutErr); return; }
  }
  /*******************************************************************/
  PaError AsyncFeed(std::vector<float> wav) {
    int BCnt, FrmCnt, PitchCnt;
    int left_phase = 0, right_phase = 0;
    unsigned long PutDexPrev, PutDexNext;
    unsigned long WavDex, wavsize = wav.size();
    unsigned long cnt, NumChunks = wavsize / FRAMES_PER_BUFFER;

    printf("AsyncFeed\n");
    this->KeepGoing = true;

    // haven't dealt with remainder!!!

    //for (cnt=0; cnt<NumChunks; cnt++){
    for (WavDex=0; WavDex<wavsize; WavDex+=FRAMES_PER_BUFFER){
      printf("  Feed loop start\n");
      PutDexPrev = this->AsyncPutDex;
      if ((PutDexNext = PutDexPrev + FRAMES_PER_BUFFER) >= AsyncRingBufSz){PutDexNext=0;}// wrap

      // WavDex = cnt;// * FRAMES_PER_BUFFER;

      // TO DO: here, copy wav[WavDex] (len=FRAMES_PER_BUFFER) to AsyncRingBuf[PutDexPrev];

      while (PutDexNext == this->AsyncGetDex){// stall until player is ready?
        //printf("fd %d, ", this->KeepGoing);
        if (!this->KeepGoing){
          printf("AsyncFeed abort\n"); break;
        }
      }
      this->AsyncPutDex = PutDexNext;
      printf("  Feed loop end\n");
    }

    /*
    goal is to loop, laying down chunks of wav onto ringbuf.
    if we can fit it all, just exit.
    if we are blocked by AsyncGetDex, have to block and wait.
    be sure and update AsyncPutDex last.

    NumChunks = wavsize DIV FRAMES_PER_BUFFER;

    for (cnt=0 to numchunks){
      while (audio->AsyncGetDex == audio->AsyncPutDex){// stall until player is ready?
      }
      PutDexPrev = audio->AsyncPutDex;
      if ((PutDexNext = PutDexPrev + FRAMES_PER_BUFFER) > AsyncRingBufSz){PutDexNext=0;}// wrap

      wavdex = cnt * FRAMES_PER_BUFFER;
      copy wav[wavdex] (len=FRAMES_PER_BUFFER) to AsyncRingBuf[PutDexPrev];
      now how about not going over AsyncGetDex?

      audio->AsyncPutDex = PutDexNext;
    }

    */
    if (false){
      unsigned long PutDex = this->AsyncPutDex;
      float left, right;
      int bufdex=0;
      int len = wav.size();
      for (int cnt=0;cnt<len;cnt++){
        left = right = wav[cnt];
        AsyncRingBuf[bufdex++] = left;
        AsyncRingBuf[bufdex++] = right;
      }
      while (this->AsyncGetDex != this->AsyncPutDex){
      }
    }
    // OutErr = Pa_WriteStream(OutStream, buffer, FRAMES_PER_BUFFER);
    return OutErr;
  }
  /*******************************************************************/
  void AsyncStop() {
    this->KeepGoing = false;
    OutErr = Pa_CloseStream(OutStream);
    if(OutErr == paNoError) { Pa_Terminate(); }
    else {// handle error?
      printf("Error closing stream.\n");
      ReportError(OutErr);
    }
  }
  /*******************************************************************/
  void ReportError(PaError OutErr) {
    fprintf(stderr, "An error occured while using the portaudio stream\n" );
    fprintf(stderr, "Error number: %d\n", OutErr);
    fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(OutErr) );
    if (OutErr == paUnanticipatedHostError) {// Print more information about the error.
      const PaHostErrorInfo *hostErrorInfo = Pa_GetLastHostErrorInfo();
      fprintf( stderr, "Host API error = #%ld, hostApiType = %d\n", hostErrorInfo->errorCode, hostErrorInfo->hostApiType );
      fprintf( stderr, "Host API error = %s\n", hostErrorInfo->errorText );
    }
    Pa_Terminate();
  }
  /*******************************************************************/
  static int Blocking() {// http://portaudio.com/docs/v19-doxydocs/paex__write__sine_8c_source.html
    PaStreamParameters OutputParameters;
    PaStream *stream;
    PaError err;
    float buffer[FRAMES_PER_BUFFER][NUM_CHANNELS]; /* stereo output buffer */
    float sine[TABLE_SIZE]; /* sine wavetable */
    int left_phase = 0, right_phase = 0;
    int left_inc = 1;
    int right_inc = 3; /* higher pitch so we can distinguish left and right. */
    int BCnt, FrmCnt, PitchCnt;
    int BufferCount;

    printf("PortAudio Test: output sine wave. SR = %d, BufSize = %d\n", SAMPLE_RATE, FRAMES_PER_BUFFER);

    /* initialise sinusoidal wavetable */
    for( BCnt=0; BCnt<TABLE_SIZE; BCnt++ ) {
      sine[BCnt] = (float) sin( ((double)BCnt/(double)TABLE_SIZE) * M_PI * 2.0 );
    }

    err = Pa_Initialize();
    if( err != paNoError ) {
      goto error;
    }

    OutputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (OutputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      goto error;
    }
    OutputParameters.channelCount = NUM_CHANNELS;       /* stereo output */
    OutputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    OutputParameters.suggestedLatency = 0.050; // Pa_GetDeviceInfo( OutputParameters.device )->defaultLowOutputLatency;
    OutputParameters.hostApiSpecificStreamInfo = NULL;

    err = Pa_OpenStream(
            &stream,
            NULL, /* no input */
            &OutputParameters,
            SAMPLE_RATE, FRAMES_PER_BUFFER,
            paClipOff,      /* we won't output out of range samples so don't bother clipping them */
            NULL, /* no callback, use blocking API */
            NULL ); /* no callback, so no callback userData */

    if( err != paNoError ) { goto error; }

    printf( "Play 3 times, higher each time.\n" );

    for(PitchCnt=0; PitchCnt < 3; ++PitchCnt) {
      err = Pa_StartStream( stream );
      if( err != paNoError ) { goto error; }

      printf("Play for %d seconds.\n", NUM_SECONDS );

      BufferCount = ((NUM_SECONDS * SAMPLE_RATE) / FRAMES_PER_BUFFER);

      printf("BufferCount:%d\n", BufferCount);
      for( BCnt=0; BCnt < BufferCount; BCnt++ ) {
        for(FrmCnt=0; FrmCnt < FRAMES_PER_BUFFER; FrmCnt++) {// create sine waves
          buffer[FrmCnt][0] = sine[left_phase];  /* left */
          buffer[FrmCnt][1] = sine[right_phase];  /* right */
          left_phase += left_inc;
          if( left_phase >= TABLE_SIZE ) {
            left_phase -= TABLE_SIZE;// wrap
          }
          right_phase += right_inc;
          if( right_phase >= TABLE_SIZE ) {
            right_phase -= TABLE_SIZE;// wrap
          }
        }

        err = Pa_WriteStream(stream, buffer, FRAMES_PER_BUFFER);
        if( err != paNoError ) { goto error; }

//        signed long writeable = Pa_GetStreamWriteAvailable(stream);
//        printf("Blocking writeable:%ld\n", writeable);
        //Pa_Sleep(10);
        /// usleep(10 * 1000);// 10 milliseconds = 1/100 second is tolerable
      }

      err = Pa_StopStream( stream );
      if( err != paNoError ) { goto error; }
      ++left_inc;
      ++right_inc;
      //break;
      Pa_Sleep(1000);
    }

    err = Pa_CloseStream( stream );
    if( err != paNoError ) {
      goto error;
    }

    Pa_Terminate();
    printf("Test finished.\n");

    return err;

error:
    fprintf( stderr, "An error occured while using the portaudio stream\n" );
    fprintf( stderr, "Error number: %d\n", err );
    fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( err ) );
    if (err == paUnanticipatedHostError) {// Print more information about the error.
      const PaHostErrorInfo *hostErrorInfo = Pa_GetLastHostErrorInfo();
      fprintf( stderr, "Host API error = #%ld, hostApiType = %d\n", hostErrorInfo->errorCode, hostErrorInfo->hostApiType );
      fprintf( stderr, "Host API error = %s\n", hostErrorInfo->errorText );
    }
    Pa_Terminate();
    return err;
  }

  /*******************************************************************/
  std::thread playthread;
  static const int ringbufsz = 1024;
  double RingBuf[ringbufsz];
  int GetDex, PutDex;
  /*******************************************************************/
  void ThreadCallback(){
    std::cout << this->AsyncGetDex << "\n";
    int cnt = 0;
    float sample;
    float buffer[FRAMES_PER_BUFFER][NUM_CHANNELS]; /* stereo output buffer */
    while (GetDex!=PutDex){
      GetDex++;// reader can overtake writer
      if (GetDex>=ringbufsz){GetDex=0;} // wrap
      sample = (float)RingBuf[GetDex];
      buffer[cnt][0] = buffer[cnt][1] = sample;// questionable, hacky
    }
    OutErr = Pa_WriteStream(OutStream, buffer, FRAMES_PER_BUFFER);
  }
  /*******************************************************************/
  void Start() override {
    PaStreamParameters OutputParameters;

    OutErr = Pa_Initialize();
    if( OutErr != paNoError ) { goto error; }

    OutputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (OutputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      goto error;
    }
    OutputParameters.channelCount = NUM_CHANNELS;       /* stereo output */
    OutputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    OutputParameters.suggestedLatency = 0.050; // Pa_GetDeviceInfo( OutputParameters.device )->defaultLowOutputLatency;
    OutputParameters.hostApiSpecificStreamInfo = NULL;

    OutErr = Pa_OpenStream(
               &OutStream,
               NULL, /* no input */
               &OutputParameters,
               SAMPLE_RATE, FRAMES_PER_BUFFER,
               paClipOff,      /* we won't output out of range samples so don't bother clipping them */
               NULL, /* no callback, use blocking API */
               NULL ); /* no callback, so no callback userData */

    if( OutErr != paNoError ) { goto error; }

    this->playthread = std::thread(&Audio::ThreadCallback, this);

error:
    fprintf( stderr, "An error occured while using the portaudio stream\n" );
    fprintf( stderr, "Error number: %d\n", OutErr);
    fprintf( stderr, "Error message: %s\n", Pa_GetErrorText(OutErr) );
    if (OutErr == paUnanticipatedHostError) {// Print more information about the error.
      const PaHostErrorInfo *hostErrorInfo = Pa_GetLastHostErrorInfo();
      fprintf( stderr, "Host API error = #%ld, hostApiType = %d\n", hostErrorInfo->errorCode, hostErrorInfo->hostApiType );
      fprintf( stderr, "Host API error = %s\n", hostErrorInfo->errorText );
    }
    Pa_Terminate();
  }
  /*******************************************************************/
  void Feed(std::vector<float> wav) override {
    int BCnt, FrmCnt, PitchCnt;
    /*
    pseudocode
    vector wav
    fpb
    chunksize = fbp
    BufferCount = math.ceil((wav.size) / fbp);
    */
    // int left_phase = 0, right_phase = 0;
    float* buffer = &wav[0];
    OutErr = Pa_WriteStream(OutStream, buffer, FRAMES_PER_BUFFER);
  }
  /*******************************************************************/
  PaError Feed(float buffer[FRAMES_PER_BUFFER][NUM_CHANNELS]) {
    int BCnt, FrmCnt, PitchCnt;
    // int left_phase = 0, right_phase = 0;
    OutErr = Pa_WriteStream(OutStream, buffer, FRAMES_PER_BUFFER);
    return OutErr;
  }
  /*******************************************************************/
  void Stop() override {
    OutErr = Pa_CloseStream(OutStream);
    if(OutErr == paNoError) { Pa_Terminate(); }
    else { }// handle error?

    if (this->playthread.joinable()){
      this->playthread.join();
    }
  }
};

#endif // AudioLab_hpp
/*
https://stackoverflow.com/questions/15033827/multiple-threads-writing-to-stdcout-or-stdcerr
std::stringstream stream; // #include <sstream> for this
stream << 1 << 2 << 3;
std::cout << stream.str();

std::stringstream Bruce; // #include <sstream> for this
Bruce << " >>>>>>>>>>>> FEED 4 >>>>>>>>>>>>>>> RingGetDex:" << this->RingGetDex << ", RingPutDex:" << this->RingPutDex << "\n";
std::cout << Bruce.str();

Next steps:
clean up all mess.
create separate, neat demo with just threads. convert data type from int to double.
creat testfeeder that generates sines.
copy thread version and install blocking portaudio calls first.
refine yet more for non-blocking portaudio calls.


PortAudio Test: output sine wave. SR = 44100, BufSize = 1024
ALSA lib pcm_dmix.c:1052:(snd_pcm_dmix_open) unable to open slave
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.rear
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.center_lfe
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.side
ALSA lib pcm_route.c:867:(find_matching_chmap) Found no matching channel map
ALSA lib pcm_dmix.c:1052:(snd_pcm_dmix_open) unable to open slave
Cannot connect to server socket err = No such file or directory
Cannot connect to server request channel
jack server is not running or cannot be started
JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock
JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock
writeable:-9976
FramesPerBuffer:96
Play for 1 seconds.
ALSA lib pcm.c:8306:(snd_pcm_recover) underrun occurred
FramesPerBuffer:96
FramesPerBuffer:96
FramesPerBuffer:96


now:1607684342
TestFeeder3(); start ******************************************************************************************************
Init()
ALSA lib pcm_dmix.c:1052:(snd_pcm_dmix_open) unable to open slave
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.rear
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.center_lfe
ALSA lib pcm.c:2495:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.side
ALSA lib pcm_route.c:867:(find_matching_chmap) Found no matching channel map
ALSA lib pcm_dmix.c:1052:(snd_pcm_dmix_open) unable to open slave
Cannot connect to server socket err = No such file or directory
Cannot connect to server request channel
jack server is not running or cannot be started
JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock
JackShmReadWritePtr::~JackShmReadWritePtr - Init not done for -1, skipping unlock
Repeater, KeepGoing:1
An error occured while using the portaudio stream
Error number: -9976
Error message: Can't write to a callback stream
Expression 'pthread_join( self->thread, &pret )' failed in 'src/os/unix/pa_unix_util.c', line: 441
Expression 'PaUnixThread_Terminate( &stream->thread, !abort, &threadRes )' failed in 'src/hostapi/alsa/pa_linux_alsa.c', line: 3102
john@FatFreddy:~/Documents/Projects/PortAudioTest/bin$

An error occured while using the portaudio stream
"-9976" "Can't write to a callback stream"


*/

