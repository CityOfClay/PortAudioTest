#ifndef Hilo_hpp
#define Hilo_hpp

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <stdalign.h>
#include <cstring>
#include <vector>
#include <thread> // std::thread
#include <sstream>
#include "portaudio.h"

#define NUM_SECONDS         (5)
#define SAMPLE_RATE         (44100)
#define FRAMES_PER_BUFFER   (1024)
#define NUM_CHANNELS   (2)

#ifndef M_PI
#define M_PI  (3.14159265)
#endif

#define TABLE_SIZE   (200) // sine table size

#include "AudioFeed.hpp"

/* ********************************************************************************* */
class Hilo : public AudioFeed {// experiment to get ring buffer right
public:
  /*******************************************************************/
  std::thread PlayThread;
  static const int Num_Channels = 1;
  static const int ChunkSize = 5;//11;//9;// 3;//7;//3;//1024;FramesPerBuf;
  static const int NumRingChunks = 3;//2;//5;//7;//11;
  static const int RingBufSz = ChunkSize * Num_Channels * NumRingChunks;
  float RingBuf[RingBufSz];
  int RingGetDex, RingPutDex; // rough-grained indexes, always mod chunksize == 0.
  int RingPutDexNext;// fine-grained put index.
  int WavNext;
  bool KeepGoing;
  /*******************************************************************/
  inline int Wrap(int Prev, int Inc, int Limit) {// not used yet
    if ((Prev += Inc) >= Limit){ return 0; }
    return Prev;
  }
  /*******************************************************************/
  inline int Wrap(int Prev, int Inc) {// not used yet
    if ((Prev += Inc) >= RingBufSz){ return 0; }
    return Prev;
  }
  /*******************************************************************/
  bool Repeater(){
    bool Talk = true;
    int RingGetDexPrev, RingGetDexNext;
    double sample;
    std::cout << "Repeater" << ", KeepGoing:" << this->KeepGoing << "\n";
    while (this->RingGetDex == this->RingPutDex) {// stall
      if (!this->KeepGoing) {
        std::cout << "Repeater abort\n";
        return false;// break;
      }
    }
    RingGetDexPrev = this->RingGetDex;
    RingGetDexNext = RingGetDexPrev + ChunkSize;// Increase by chunk size.

    std::stringstream Bruce; // #include <sstream> for this
    Bruce << " OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO RingGetDexNext:" << RingGetDexNext << "\n";// to do: read data here
    std::cout << Bruce.str();

    if (Talk) {
      std::cout << "***** Reader Chunk: " << "\n";
    }
    for (int pcnt=RingGetDexPrev; pcnt<RingGetDexNext; pcnt++){// print data
      if (Talk) {
        // std::cout << "READER pcnt:[" << pcnt << "] RingBuf:(" << RingBuf[pcnt] << ")\n";// to do: read data here
        std::stringstream Bruce; // #include <sstream> for this
        Bruce << "READER pcnt:[" << pcnt << "] RingBuf:(" << RingBuf[pcnt] << ")\n";// to do: read data here
        std::cout << Bruce.str();
      }
    }
    if ((RingGetDexNext) >= RingBufSz){RingGetDexNext=0;}// wrap
    this->RingGetDex = RingGetDexNext;
    return true;
  }
  /*******************************************************************/
  void ThreadCallback(){
    //std::cout << "Starting background worker.\n";
    //usleep(1 * 100 * 1000);// 0.1 second
    //usleep(1 * 1000 * 1000);// 1 second
    while (Repeater()){}
    std::cout << "Exiting background worker.\n";
  }
  /*******************************************************************/
  void Start() override {
    this->Init();
    for (int cnt=0;cnt<RingBufSz; cnt++) {
      RingBuf[cnt] = 99999;
    }
    this->PlayThread = std::thread(&Hilo::ThreadCallback, this);
    std::cout << "Start Threaded!\n";
  }
  /*******************************************************************/
  void Init() {
    this->KeepGoing = true;
    this->WavNext = ChunkSize;
    this->RingGetDex = this->RingPutDex = 0; // rough-grained indexes, always mod chunksize == 0.
    this->RingPutDexNext = 0;// fine-grained put index.
    std::cout << "Init() \n";
  }
  /*******************************************************************/
  void Stop() override {
    this->Drain();
    this->UnInit();
    if (this->PlayThread.joinable()){
      std::cout << "this->PlayThread.join() \n";
      this->PlayThread.join();
    }
  }
  /*******************************************************************/
  void UnInit() {
    this->KeepGoing = false;
    std::cout << "UnInit() " << ", KeepGoing:" << this->KeepGoing << "\n";
  }
  /*******************************************************************/
  void Feed(std::vector<float> wave) override {// Think of this as a loop.  Integrates ring buffer and odd-sized wave vectors.
    // At this point WavNext should be the offset from the beginning of the array that we run up *to*
    // WavNext *can* be beyond the end of the next array, if next array length is < ChunkSize.
    double value;
    int RingCnt = this->RingPutDexNext;// A dereference for RingPutDexNext.
    int WavPrev = 0, WavNext, WavDex;
    int WavSize = wave.size();// Only need to dereference wave size once.
    while (this->WavNext <= WavSize) {
      for (WavDex = WavPrev; WavDex<this->WavNext; WavDex++) {// copy chunk, or chunklet inherited from previous call.
        value = wave[WavDex];
        RingBuf[RingCnt++] = value;
      }
      WavPrev = this->WavNext;
      this->WavNext += ChunkSize;
      if (RingCnt >= RingBufSz) { RingCnt = 0; } // wrap
      while (RingCnt == this->RingGetDex){// Stall until player is ready.
        // to do: put timeout condition to prevent lockup?
      }
      this->RingPutDex = RingCnt;
    }
    for (WavDex = WavPrev; WavDex<WavSize; WavDex++) {// copy remainder, last bit of array, which is less than ChunkSize long.
      value = wave[WavDex];
      RingBuf[RingCnt++] = value;
    }
    this->RingPutDexNext = RingCnt;
    this->WavNext -= WavSize; // After end of remainder, set up index for next call to Feed().
  }
  /*******************************************************************/
  void Drain() {// Close out last bit of odd-sized remainder.
    if (this->WavNext < ChunkSize) {
      int End = this->RingPutDexNext + this->WavNext;
      for (int rcnt=this->RingPutDexNext; rcnt<End; rcnt++) { RingBuf[rcnt] = 0; }// Pad with zeros.
      if (End >= RingBufSz) { End=0; }// wrap
      while (this->RingGetDex == End) {// stall.  RingPutDex is not allowed to overtake RingGetDex.
        // to do: put timeout condition to prevent lockup?
      }
      this->RingPutDex = End;
    }
  }
};

/*******************************************************************/
class Hilo_Wave : public AudioFeed {// Experiment to get wave chopping right, NO threading.
public:
  static const int Num_Channels = 1;
  static const int ChunkSize = 5;//11;//9;// 3;//7;//3;//1024;FramesPerBuf;
  int WavNext;
  /*******************************************************************/
  void Start() override {
    this->WavNext = ChunkSize;
    std::cout << "Start Non-threaded!\n";
  }
  /*******************************************************************/
  void Stop() override {
    std::cout << "Stop()\n";
  }
  /*******************************************************************/
  void Feed(std::vector<float> wave) override {// Think of this as a loop.  Strictly about wave vectors, no dealing with ring buffer.
    // at this point WavNext should be the offset from the beginning of the array that we run up *to*
    // WavNext *can* be beyond the end of the next array, if next array length is < ChunkSize.
    int RingPutDexPrev;
    int WavPrev = 0;
    int dex;
    double value;
    int WavSize = wave.size();// Only need to dereference wave size once.

    std::cout << " >>>>>>>>>>>>>>>>>>>>> Begin Feed2, wavsize:" << WavSize << ", WavNext:" << this->WavNext << " \n" ;
    while (this->WavNext <= WavSize) {
      std::cout << "****** Chunk! \n";
      for (dex = WavPrev; dex<this->WavNext; dex++) {// copy chunk, or chunklet.
        value = wave[dex];
        // RingBuf[RingCnt++] = wave[dex];// Need to offset RingPutDexPrev.
        // for now, just print wave[dex] here, without threads.
        std::cout << "chunk  value:" << value << "\n";
      }
      WavPrev = this->WavNext;
      this->WavNext += ChunkSize;
    }
    if (WavPrev < WavSize) {
      std::cout << "****** Chunk Remainder! " << " WavPrev:" << WavPrev << ", WavSize:" << WavSize <<  "\n";
    }
    // put remainder here.
    for (dex = WavPrev; dex<WavSize; dex++) {// copy last bit of array, which is less than ChunkSize long.
      value = wave[dex];
      // RingBuf[RingCnt++] = wave[dex];// Need to offset RingPutDexPrev.
      // never close as 'written' here.  only in drain().
      // for now, just print wave[dex] here, without threads.
      std::cout << "remain value:" << value << "\n";
    }
    this->WavNext -= WavSize; // beyond remainder, index for next call to Feed()
    std::cout << " >>>>>>>>>>>>>>>>>>>>> End Feed2 \n";
  }
};

/* ********************************************************************************* */
class Hilo_Ring : public AudioFeed {// experiment to get ring buffer right
public:
  /*******************************************************************/
  std::thread PlayThread;
  static const int Num_Channels = 1;
  static const int FramesPerBuf = 3;//3;//1024;
  static const int ChunkSize = FramesPerBuf;
  static const int RingBufSz = FramesPerBuf * Num_Channels * 7;//11;
  double RingBuf[RingBufSz];
  int RingGetDex, RingPutDex;
  bool KeepGoing;
  int WavDexPrev;
  int WavNext;
  /*******************************************************************/
  inline int Wrap(int Prev, int Inc, int Limit) {// not used yet
    if ((Prev += Inc) >= Limit){ return 0; }
    return Prev;
  }
  /*******************************************************************/
  bool Repeater() {// This came from hilo proper.
    int RingGetDexPrev, RingGetDexNext;
    double sample;
    std::cout << "Repeater" << ", KeepGoing:" << this->KeepGoing << "\n";
    while (this->RingGetDex == this->RingPutDex) {// stall
      if (!this->KeepGoing) {
        std::cout << "Repeater abort\n";
        return false;// break;
      }
    }
    RingGetDexPrev = this->RingGetDex;
    RingGetDexNext = RingGetDexPrev + ChunkSize;// Increase by chunk size.
    for (int pcnt=RingGetDexPrev; pcnt<RingGetDexNext; pcnt++){// print data
      // std::cout << "READER pcnt:[" << pcnt << "] RingBuf:(" << RingBuf[pcnt] << ")\n";// to do: read data here
      std::stringstream Bruce; // #include <sstream> for this
      Bruce << "READER pcnt:[" << pcnt << "] RingBuf:(" << RingBuf[pcnt] << ")\n";// to do: read data here
      std::cout << Bruce.str();
    }
    if ((RingGetDexNext) >= RingBufSz){RingGetDexNext=0;}// wrap
    this->RingGetDex = RingGetDexNext;
    return true;
  }
  /*******************************************************************/
  void ThreadCallback(){
    std::cout << "Starting background worker.\n";
    while (Repeater()){}
    std::cout << "Exiting background worker.\n";
  }
  /*******************************************************************/
  void Start() override {
    this->Init();
    for (int cnt=0;cnt<RingBufSz; cnt++) {
      RingBuf[cnt] = 99999;
    }
    this->PlayThread = std::thread(&Hilo_Ring::ThreadCallback, this);
    std::cout << "Start Threaded!\n";
  }
  /*******************************************************************/
  void Init() {
    this->KeepGoing = true;
    this->WavDexPrev = 0;
    this->WavNext = ChunkSize;
    this->RingGetDex = this->RingPutDex = 0; // rough-grained indexes, always mod chunksize == 0.
    std::cout << "Init() \n";
  }
  /*******************************************************************/
  void Feed(std::vector<float> wav) override {// Strictly ring buffer, no dealing with odd-sized wave vectors.
    int RingPutDexPrev, RingPutDexNext, WavDexPrev, WavDexNext;
    int WavSize = wav.size();
    int WavLimitChopped = ChunkSize * ((this->WavDexPrev + WavSize)/ChunkSize);// chop off remainder
    int WavSizeChopped = ChunkSize * (WavSize/ChunkSize);// chop off remainder
    int RingCnt = 0;
    WavDexPrev = 0;
    RingPutDexNext = 0;

    // chunk size bigger than 1
    std::cout << "WavSizeChopped:" << WavSizeChopped << "\n";
    for (WavDexNext=ChunkSize; WavDexNext<=WavSizeChopped; WavDexNext+=ChunkSize) {
      RingCnt = RingPutDexPrev = this->RingPutDex;

      if ((RingPutDexNext = RingPutDexPrev + ChunkSize) >= RingBufSz){RingPutDexNext=0;}// wrap
      while (RingPutDexNext == this->RingGetDex){// Stall until player is ready.
        // to do: put timeout condition to prevent lockup?
      }

      // usleep(1 * 511 * 1000);
      for (int WavCnt=this->WavDexPrev; WavCnt<WavDexNext; WavCnt++){// copy part
        RingBuf[RingCnt++] = wav[WavCnt];// Need to offset RingPutDexPrev.
      }

      this->RingPutDex = RingPutDexNext;
      this->WavDexPrev = WavDexNext;
    }
  }
  /*******************************************************************/
  void Stop() override {
    this->UnInit();
    if (this->PlayThread.joinable()){
      std::cout << "this->PlayThread.join() \n";
      this->PlayThread.join();
    }
  }
  /*******************************************************************/
  void UnInit() {
    this->KeepGoing = false;
    std::cout << "UnInit() " << ", KeepGoing:" << this->KeepGoing << "\n";
  }
};

#endif // Hilo_hpp
/*
https://stackoverflow.com/questions/15033827/multiple-threads-writing-to-stdcout-or-stdcerr
std::stringstream stream; // #include <sstream> for this
stream << 1 << 2 << 3;
std::cout << stream.str();

std::stringstream Bruce; // #include <sstream> for this
Bruce << " >>>>>>>>>>>> FEED 4 >>>>>>>>>>>>>>> RingGetDex:" << this->RingGetDex << ", RingPutDex:" << this->RingPutDex << "\n";
std::cout << Bruce.str();

Next steps:
clean up all mess.
create separate, neat demo with just threads. convert data type from int to double.
creat testfeeder that generates sines.
copy thread version and install blocking portaudio calls first.
refine yet more for non-blocking portaudio calls.

*/

